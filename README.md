# README #

This repo contains a set of bibliography citation style files for the [OpenOffice plugin for jabref](http://jabref.sourceforge.net/OOPlugin-jabref.php) which is useful for creating scientific manuscripts using the open-source LibreOffice or OpenOffice.

I collected an initial set of files from: http://jabref.sourceforge.net/OOPlugin-styles.php  These contained:

- Brazilian Journal of Aquatic Science and Technology
- Advanced Materials
- American Mineralogist
- Chemical Geology	
- Geochim Cosmochim Acta	
- Journal of Materials Chemistry
- Journal of Neurophysiology
- "Thesis, example style with numbered citations
- "Turabian" (Deutsch)
- "Turabian" (English)

Additionally I have added:

- Limnology and Oceanography
- Limnology and Oceanography: Methods
- Public Library of Science One (PLoS one)
- Environmental Microbiology
- Environmental Microbiology reports

Feel free to use them, but at your own risk. I also welcome corrections, improvements or additions to this list.