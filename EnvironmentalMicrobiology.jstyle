 # This file is licensed under the terms of the GPL version 2 (or later).
 # Created by Arjun Chennu
 # Style formatting derived from: http://aslo.org/lo/instructions/authors.html

NAME
Environmental Microbiology citation style 

JOURNALS
Environmental Microbiology
Environmental Microbiology Reports

PROPERTIES
Title=References
IsSortByPosition=false
IsNumberEntries=false
ReferenceParagraphFormat="Hanging Indent"
ReferenceHeaderParagraphFormat="Heading 1"

CITATION
AuthorField=author/editor
YearField=year
MaxAuthors=2
AuthorSeparator=, 
AuthorLastSeparator= and 
EtAlString= et al.
YearSeparator=, 
InTextYearSeparator= 
BracketBefore=(
BracketAfter=)
BracketBeforeInList=[
BracketAfterInList=]
CitationSeparator=; 
UniquefierSeparator=,
GroupedNumbersSeparator=-
MinimumGroupingCount=3
FormatCitations=false
ItalicCitations=false
BoldCitations=false
SuperscriptCitations=false
SubscriptCitations=false


LAYOUT

article=\format[AuthorLastFirstAbbreviator,Authors(LastFirst,Initials,FullPunc,Comma,7,6),NoSpaceBetweenAbbreviations,Replace( &, and)]{\author} (\format[FormatChars]{\year}\uniq) \format[HTMLChars,FormatChars]{\title}. \format[JournalAbbreviator]{\journal} \volume: \format[FormatPagesForHTML]{\pages}.\begin{doi} [doi:\format[FormatChars]{\doi}]\end{doi}

book=\format[AuthorLastFirstAbbreviator,Authors(LastFirst,Initials,FullPunc,Comma,7,6),NoSpaceBetweenAbbreviations,AuthorAndsCommaReplacer,Replace( &, and)]{\author} (\format[FormatChars]\year\uniq) \format[FormatChars]{\title}\begin{edition}, \format[FormatChars]{\edition}\end{edition}.\begin{publisher} \format[FormatChars]{\publisher}\end{publisher}.

incollection=\format[AuthorLastFirstAbbreviator,Authors(LastFirst,Initials,FullPunc,Comma,7,6),NoSpaceBetweenAbbreviations,AuthorAndsCommaReplacer,Replace( &, and)]{\author} (\year\uniq) \format[FormatChars]{\title}. \format[FormatChars]{\booktitle}. \begin{editor}\format[AuthorLastFirstAbbreviator,Authors(LastFirstFirstFirst),NoSpaceBetweenAbbreviations,AuthorAndsCommaReplacer,Replace( &, and)]{\editor} (\format[IfPlural(eds,ed)]{\editor}).\end{editor} \format[FormatChars]{\publisher}\begin{pages}, pp.\format[FormatPagesForHTML]{\pages}\end{pages}.\begin{doi} [doi:\format[FormatChars]{\doi}]\end{doi}

inbook=\format[AuthorLastFirstAbbreviator,Authors(LastFirst,Initials,FullPunc,Comma,7,6),NoSpaceBetweenAbbreviations,AuthorAndsCommaReplacer,Replace( &, and)]{\author} (\year\uniq) \format[FormatChars]{\title}. \format[FormatChars]{\booktitle}. \begin{editor} \format[AuthorLastFirstAbbreviator,Authors(LastFirstFirstFirst),NoSpaceBetweenAbbreviations,AuthorAndsCommaReplacer,Replace( &, and)]{\editor} (\format[IfPlural(eds,ed)]{\editor})\end{editor}. \format[FormatChars]{\publisher}.\begin{doi} [doi:\format[FormatChars]{\doi}]\end{doi}


default=\format[AuthorLastFirstAbbreviator,Authors(LastFirst,Initials,FullPunc,Comma,7,6),NoSpaceBetweenAbbreviations,AuthorAndsCommaReplacer,Replace( &, and)]{\author} \year\uniq. \format[FormatChars]{\title}. \journal <b>\volume</b>: \format[FormatPagesForHTML]{\pages}.\begin{doi} [doi:\format[FormatChars]{\doi}]\end{doi}



